// part1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <opencv2\core\optim.hpp>
#include "opencv2\opencv.hpp"
#include "opencv2\imgcodecs.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"

#include <array>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <algorithm>

// the number of coefficients
const int num_coeffs_x = 4;
const int num_coeffs_y = 4;

// Defining a CylindricalF class which has the function we want to minimize---------------------------------------------------------------------------------------------------------------------------
class CylindricalF : public cv::MinProblemSolver::Function
{
public:
	cv::Mat topFeat_co; // creating a vector to hold all the coeffs since we cant just have 11 separate vars to hold the coeffs limit is 10
	cv::Mat bottomFeat_co;
	cv::Mat rightFeat_co;
	cv::Mat leftFeat_co;

	int getDims() const { return num_coeffs_x + num_coeffs_y; };

	double calc(const double* c) const
	{
		double totSum(0);

		//cv::Mat coeffs(num_coeffs_x + num_coeffs_y, 1, CV_64FC1, *c);
		cv::Mat coeffs(std::vector<double>(c, c + num_coeffs_x+num_coeffs_y), true);

		cv::Mat xcoeffs = coeffs.rowRange(0, num_coeffs_x);
		cv::Mat ycoeffs = coeffs.rowRange(num_coeffs_x, num_coeffs_x+num_coeffs_y);

		double topSum(0);
		double bottomSum(0);
		double leftSum(0);
		double rightSum(0);
		
		cv::Mat err;

		err = (leftFeat_co * coeffs.rowRange(0, num_coeffs_x));
		leftSum = err.dot(err) + 0;

		err = (rightFeat_co * coeffs.rowRange(0, num_coeffs_x) - 1);
		rightSum = err.dot(err) + 0;
	
		err = (topFeat_co * coeffs.rowRange(num_coeffs_x, num_coeffs_x + num_coeffs_y) - 1);
		topSum = err.dot(err) + 0;

		err = (bottomFeat_co * coeffs.rowRange(num_coeffs_x, num_coeffs_x + num_coeffs_y));
		bottomSum = err.dot(err) + 0 ;
	
		totSum = topSum + bottomSum + rightSum + leftSum;

		return totSum;
	}


	CylindricalF(cv::Mat topFeat, cv::Mat bottomFeat, cv::Mat rightFeat, cv::Mat leftFeat)
	{
		if (bottomFeat.cols == num_coeffs_y && topFeat.cols == num_coeffs_y && rightFeat.cols == num_coeffs_x && leftFeat.cols == num_coeffs_x)
		{
			bottomFeat.convertTo(bottomFeat_co,CV_64F);
			topFeat.convertTo(topFeat_co,CV_64F);
			rightFeat.convertTo(rightFeat_co,CV_64F);
			leftFeat.convertTo(leftFeat_co,CV_64F);
		}
		else
		{
			std::cout << "Error : (line 70) Not enough cols in the feature vector " << std::endl;
		}
	}
};

// Function to find the largest connected component in a given image
cv::Mat findLargestCC(const cv::Mat& src) 
{
	cv::Mat labels;
	cv::Mat stats;
	cv::Mat centroids;
	int nLabels = cv::connectedComponentsWithStats(src, labels, stats, centroids, 8, CV_32S);

	// stats - will be a (5 x nLabels) matrix with  each column as the following properties (left,top, widhth,height, area )
	cv::Mat stat_areas = stats.col(cv::CC_STAT_AREA).rowRange(1, nLabels ); // obtaining the area stats and ignoring the background area value

	cv::Point maxAreaLoc; 

	cv::minMaxLoc(stat_areas, nullptr, nullptr, nullptr,&maxAreaLoc);

	cv::Mat largestCC = (labels == maxAreaLoc.y + 1); // the plus one is because the 0 valued label is the backgrounds label
	
	return largestCC;
}

// Function to apply filtering operation and then finding the largest connected component
cv::Mat largestCCWithKernel(const cv::Mat& src , const cv::Mat& BorderKernel) {
	cv::Mat border;
	cv::filter2D(src, border, -1, BorderKernel, cv::Point(-1, -1), 0, cv::BORDER_DEFAULT);
	
	return findLargestCC(border); 
}

// Function to calculate the feature values
cv::Mat y_features(int y, int x)
{
	cv::Mat yFeats = (cv::Mat_<float>(1,num_coeffs_y) << 1,x,y, pow(x,2));
	return yFeats;
}

cv::Mat x_features(int y, int x)
{
	cv::Mat xFeats = (cv::Mat_<float>(1, num_coeffs_x) << 1, y, x, x*y);
	return xFeats;
}



void setSolverStuff(cv::Ptr<cv::DownhillSolver> solver, cv::Ptr<cv::MinProblemSolver::Function> ptr_F, cv::Mat& x_initCentroid, cv::Mat& step, int useTermCrit)
{
	solver->setFunction(ptr_F); // solver is a DownhillSolver object pointer used to set and use all the basic DownhillSolver public member vars and member funcs  
	solver->setInitStep(step); // Sets the initial step that will be used in downhill simplex algorithm. 
	cv::TermCriteria termCrit;

	if (useTermCrit == 1) {
		termCrit = solver->getTermCriteria();
		termCrit.type = cv::TermCriteria::COUNT + cv::TermCriteria::EPS;
		termCrit.maxCount = INT_MAX;
		termCrit.epsilon = FLT_EPSILON;

		solver->setTermCriteria(termCrit);

	}

	double result = solver->minimize(x_initCentroid);  // actually runs the algorithm and performs the minimization.
													   // The sole input parameter ' x_init_centroid'  determines the centroid of the starting simplex(roughly, it tells where to start)
													   // result - holds the value of the function to be minimized after the minimization 
}

//===========================================================  MAIN  ==============================================================
int main(int argc, char* argv[])
{
	if (argc < 3)
	{
		std::cout << "ERROR : program_name image_name mask_name" << std::endl;
		return -1;
	}

	cv::Mat Img_org;
	cv::Mat Img_mask;

	Img_org = cv::imread(argv[1]);
	Img_mask = 255 * (cv::imread(argv[2], cv::IMREAD_GRAYSCALE) >  100);

	cv::imshow("the orig img", Img_org);

	if (Img_mask.empty() || Img_org.empty())
	{
		std::cout << "ERROR : Images not loaded " << std::endl;
		return -2;
	}

	double img_height = Img_org.rows;
	double img_width  = Img_org.cols;

	//Kernels for finding the sides of the label
	cv::Mat topBorderKernel    = (cv::Mat_<float>(2, 1) << -1.0,  1.0);
	cv::Mat bottomBorderKernel = (cv::Mat_<float>(2, 1) <<  1.0, -1.0);
	cv::Mat rightBorderKernel  = (cv::Mat_<float>(1, 2) <<  1.0, -1.0);
	cv::Mat leftBorderKernel   = (cv::Mat_<float>(1, 2) << -1.0,  1.0);

	// The border labels (binary) 0 or 255
	cv::Mat topBorderLabels    = largestCCWithKernel(Img_mask, topBorderKernel);
	cv::Mat bottomBorderLabels = largestCCWithKernel(Img_mask, bottomBorderKernel);
	cv::Mat rightBorderLabels  = largestCCWithKernel(Img_mask, rightBorderKernel);
	cv::Mat leftBorderLabels   = largestCCWithKernel(Img_mask, leftBorderKernel);

	// Creating the feature matrix
	cv::Mat topFeatures(cv::countNonZero(topBorderLabels), num_coeffs_y, CV_32F, cv::Scalar::all(0));
	cv::Mat bottomFeatures(cv::countNonZero(bottomBorderLabels), num_coeffs_y, CV_32F, cv::Scalar::all(0));
	cv::Mat rightFeatures(cv::countNonZero(rightBorderLabels), num_coeffs_x, CV_32F, cv::Scalar::all(0));
	cv::Mat leftFeatures(cv::countNonZero(leftBorderLabels), num_coeffs_x, CV_32F, cv::Scalar::all(0));

	int topBorderLabels_idx = 0;
	int bottomBorderLabels_idx = 0; 
	int rightBorderLabels_idx = 0;
	int leftBorderLabels_idx = 0;
	
	// Finding all the features 
	for (int y = 0; y<Img_mask.rows; ++y)
	{
		for (int x = 0; x<Img_mask.cols; ++x)
		{
			if (topBorderLabels.at<unsigned char>(y, x) != 0)
			{
				topFeatures.row(topBorderLabels_idx++) = y_features(y,x) + 0;
			}

			if (bottomBorderLabels.at<unsigned char>(y, x) != 0)
			{
				bottomFeatures.row(bottomBorderLabels_idx++) = y_features(y, x) + 0;
			}

			if (rightBorderLabels.at<unsigned char>(y, x) != 0)
			{
				rightFeatures.row(rightBorderLabels_idx++) = x_features(y, x) + 0;
			}

			if (leftBorderLabels.at<unsigned char>(y, x) != 0)
			{
				leftFeatures.row(leftBorderLabels_idx++) = x_features(y, x) + 0;
			}
		}
	}


	//-----------------------------------Applying the Downhill Solver to find the Solution------------------------------
	cv::Ptr<cv::DownhillSolver> solver = cv::DownhillSolver::create();
	cv::Ptr<cv::MinProblemSolver::Function> ptr_F = cv::makePtr<CylindricalF>(topFeatures, bottomFeatures, rightFeatures, leftFeatures);

	cv::Mat    x =  0.0*(cv::Mat_<double>(1, num_coeffs_x + num_coeffs_y) << 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0); // initializing the starting centroid of the simplex (initial point)
	cv::Mat step = 10.0*(cv::Mat_<double>(num_coeffs_x + num_coeffs_y, 1) << 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0); // the sizes of the simplex
	
	setSolverStuff(solver, ptr_F, x, step, 1);     // Initializing the solver and running the minimization			

	//---------------------------------------Applying the transform the image-----------------------------------
	float x_scaleFactor = 1.25;
	float y_scaleFactor = 1.25;

	cv::Mat txmedImg(cv::Size(x_scaleFactor * img_width, y_scaleFactor * img_height), Img_org.type());
	cv::Mat txmedMask(cv::Size(x_scaleFactor * img_width, y_scaleFactor * img_height), Img_mask.type());
	

	cv::Mat x_map(cv::Size( x_scaleFactor * img_width, y_scaleFactor * img_height), CV_32FC1);
	cv::Mat y_map(cv::Size( x_scaleFactor * img_width, y_scaleFactor * img_height), CV_32FC1);
	
	x.convertTo(x, CV_32FC1);

	cv::Mat x_coeffs = x.colRange(0, num_coeffs_x);
	cv::Mat y_coeffs = x.colRange(num_coeffs_x, num_coeffs_x + num_coeffs_y);
	//cv::Mat x_feats, y_feats;

	// Creating the map matrices to use for the transformation in remap()
	for (int x_idx = 0 ; x_idx < txmedImg.cols; x_idx++)
	{
		for (int y_idx = 0; y_idx < txmedImg.rows; y_idx++)
		{
			x_map.at<float>(y_idx, x_idx) = x_coeffs.dot(x_features(txmedImg.rows - y_idx, x_idx));
			y_map.at<float>(y_idx, x_idx) = y_coeffs.dot(y_features(txmedImg.rows - y_idx, x_idx));
		}
	}

	cv::normalize(x_map, x_map, 0, txmedImg.cols, cv::NORM_MINMAX, CV_32FC1);
	cv::normalize(y_map, y_map, 0, txmedImg.rows, cv::NORM_MINMAX, CV_32FC1);

	cv::remap(Img_org, txmedImg, x_map, y_map, cv::INTER_LINEAR, cv::BORDER_REPLICATE, cv::Scalar(0, 0, 0)); // flattening image
	cv::remap(Img_mask, txmedMask, x_map, y_map, cv::INTER_LINEAR, cv::BORDER_REPLICATE, cv::Scalar(0)); // flattening mask

	txmedMask = (txmedMask > (255/2));

	// Display the transform results
	//cv::imshow("original", Img_org);
	//cv::imshow("corrected", txmedImg);
	//cv::imshow("corrected mask", txmedMask);
	
	//cv::imwrite("flattenedImg12.jpg", txmedImg);

	// Doing masking using the flattened mask
	cv::Mat txmed_masked;
	txmedImg.copyTo(txmed_masked, txmedMask);
	//cv::imshow("masked label", txmed_masked);

	//-----------------------------------------------------SECTION TO PERFORM MORPHOLOGICAL AND OTHER PREPROCESSING ENHANCEMENTS----------------------------------------
	
	// Performing a coarse blurring of the image to figure out the lighting informantion
	if (1)
	{
		cv::Mat masked_blurred;
		cv::Mat masked_gray;

		//cv::cvtColor(txmed_masked, masked_gray, cv::COLOR_BGR2GRAY);
		cv::cvtColor(txmedImg, masked_gray, cv::COLOR_BGR2GRAY);
		cv::imshow("the gray masked image", masked_gray);
		
		//cv::medianBlur(masked_open, masked_open, 15);
		cv::GaussianBlur(masked_gray, masked_blurred, cv::Size(127, 127), 100, 100, cv::BORDER_REPLICATE); // blur kernel size should not be an even number here
		//cv::blur(masked_gray, masked_blurred, cv::Size(127, 127), cv::Point(-1, -1), cv::BORDER_REPLICATE);
		cv::imshow("blurred imasked image", masked_blurred);
		masked_blurred = 255 - masked_blurred;
		masked_blurred.convertTo(masked_blurred,CV_64FC1);
		cv::normalize(masked_blurred, masked_blurred, .01, 1, cv::NORM_MINMAX,CV_64FC1);
		//masked_blurred = 1.25 - masked_blurred; // inverting the image

		masked_gray.mul(1/masked_blurred);
		cv::imshow("The corrected gray image ", masked_gray);

		// Masking out everything except the label
		cv::Mat corrected_masked;
		masked_gray.copyTo(corrected_masked, txmedMask);
		cv::imshow("The corrected gray image of the label", corrected_masked);
	}

	cv::waitKey();
	std::cout << "\n********************************Excecution over********************************\n";
	short temp;
	std::cin >> temp;
	return 0;
}