#pragma once
#include "opencv2/opencv.hpp"


namespace LabelStuff
{

	void filterLargestConnectedComponent(cv::Mat& Img_mask, cv::Mat BorderKernel, cv::Mat& BorderLabels);
	void idx2zeroone(cv::Mat& BorderLabels, cv::Mat& NonZeroCoords, cv::Mat& WorldCoord_x, cv::Mat WorldCoord_y);

	void setSolverStuff(cv::Ptr<cv::DownhillSolver> solver, cv::Ptr<cv::MinProblemSolver::Function> ptr_F, cv::Mat& x_initCentroid, cv::Mat& step, cv::TermCriteria termCrit, int useTermCrit);
	
	void cylTransform(cv::Mat& NonZeroCoords, cv::Mat& coeff, cv::Mat& WorldCoord_x, cv::Mat& WorldCoord_y, cv::Mat& TxmedCoords_x, cv::Mat& TxmedCoords_y);
	
}