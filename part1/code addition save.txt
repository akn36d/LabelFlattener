double x_mapping(int y, int x, const cv::Mat& coeff)
{
	cv::Mat x_ = x_features(y, x);
	cv::Mat c = coeff.colRange(0, num_coeffs_x);

	return c.dot(x_);
}

double y_mapping(int y, int x, const cv::Mat& coeff)
{
	cv::Mat y_ = y_features(y, x);
	cv::Mat c = coeff.colRange(num_coeffs_y, num_coeffs_x + num_coeffs_y);

	return c.dot(y_);
}

MAIN() STUFF================================================================================================================
{
	
	//---------------------------First test implementation of the openCV minimization------------------------------------------------------------------
	cv::Ptr<cv::DownhillSolver> solver = cv::DownhillSolver::create(); // creating a downhill solver object				  
	cv::Ptr<cv::MinProblemSolver::Function> ptr_F = cv::makePtr<CylindricalF>(topFeatures, bottomFeatures, rightFeatures, leftFeatures); // creating a cv::Ptr to the class CylindricalF
	cv::Mat txm_coeffs = 0.0*cv::Mat::ones(cv::Size(num_coeffs_x + num_coeffs_y,1), CV_64F); // initializing the starting centroid of the simplex (initial point)
	cv::Mat step = 10.0*cv::Mat::ones(cv::Size(num_coeffs_x + num_coeffs_y,1), CV_64F); // the sizes of the simplex
	
	setSolverStuff(solver, ptr_F, txm_coeffs, step, 1);

	// The Mathematica Coefficients
	cv::Mat txm_coeffs_mathematica = (cv::Mat_<double>(1, 8) << 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0); // initializing the starting centroid of the simplex (initial point)
	txm_coeffs_mathematica.at<double>(0) = -0.168207;
	txm_coeffs_mathematica.at<double>(1) = 0.00426421;
	txm_coeffs_mathematica.at<double>(2) = 0.000173044;
	txm_coeffs_mathematica.at<double>(3) = -1.42875 * powf(10, -6);
	txm_coeffs_mathematica.at<double>(4) = -0.253582;
	txm_coeffs_mathematica.at<double>(5) = 0.00466717;
	txm_coeffs_mathematica.at<double>(6) = 0.00175828;
	txm_coeffs_mathematica.at<double>(7) = -6.18825 * powf(10, -6);

	std::cout << "\n The mathematica coeff: \n" << txm_coeffs_mathematica.t() << std::endl;
	
	if (0) // the coeffs from mathematica
	{
		txm_coeffs = txm_coeffs_mathematica;
	}

	

	txm_coeffs.convertTo(txm_coeffs, CV_32F);

	//----------------------------------------------------------------------------
	// Applying the transformation on the image

	float K = 1.25;
	cv::Mat txmedImg(cv::Size(K*Img_org.cols, K*Img_org.rows), Img_org.type());

	cv::Mat x_prime(txmedImg.rows, txmedImg.cols, CV_32FC1);
	cv::Mat y_prime(txmedImg.rows, txmedImg.cols, CV_32FC1);

	for (int jj = 0; jj < txmedImg.rows; ++jj)
	{
		for (int ii = 0; ii < txmedImg.cols; ++ii)
		{
			x_prime.at<float>(jj, ii) = x_mapping(jj, ii, txm_coeffs);
			y_prime.at<float>(jj, ii) = y_mapping(jj, ii, txm_coeffs);


			// Test scaling mby remapping
			//x_prime.at<float>(jj, ii) = 1 / K*ii;
			//y_prime.at<float>(jj, ii) = 1 / K*jj;
		}
	}

	cv::normalize(x_prime, x_prime, 0, txmedImg.cols, cv::NORM_MINMAX, CV_32FC1);
	cv::normalize(y_prime, y_prime, 0, txmedImg.rows, cv::NORM_MINMAX, CV_32FC1);

	cv::remap(Img_org, txmedImg, x_prime, y_prime, cv::INTER_CUBIC, cv::BORDER_CONSTANT, cv::Scalar(0, 255, 0));

	//---------------------------Finally, display results-------------------------------------------------------------------------------
	cv::imshow("original", Img_org);
	cv::imshow("corrected", txmedImg);

	cv::imwrite("output.jpg", txmedImg);
}