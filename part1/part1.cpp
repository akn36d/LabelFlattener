// part1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

//#include "LabelStuff.h"

#include <opencv2\core\optim.hpp>
#include "opencv2\opencv.hpp"
#include "opencv2\imgcodecs.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <algorithm>



//-----------------------Solver Class-----------------------------
// Defining a CylindricalF class which has the function we want to minimize---------------------------------------------------------------------------------------------------------------------------
class CylindricalF : public cv::MinProblemSolver::Function
{
public:
	cv::Mat topFeat_co; // creating a vector to hold all the coeffs since we cant just have 11 separate vars to hold the coeffs limit is 10
	cv::Mat bottomFeat_co;
	cv::Mat rightFeat_co;
	cv::Mat leftFeat_co;

	int getDims() const { return 8; };

	double calc(const double* c) const
	{
		double topTotalCostFunc(0);
		double bottomTotalCostFunc(0);
		double rightTotalCostFunc(0);
		double leftTotalCostFunc(0);
		
		double totSum(0);
		
		//cv::Mat coeffs(8, 1, CV_64FC1, &c);
		cv::Mat coeffs(std::vector<double>(c, c + 8), true);

		cv::Mat xcoeffs = coeffs.rowRange(0,4);
		cv::Mat ycoeffs = coeffs.rowRange(4,8);

		cv::Mat err;
		err = (topFeat_co * ycoeffs - 1);
		totSum += err.dot(err);

		err = (bottomFeat_co * ycoeffs);
		totSum += err.dot(err);
		
		err = (leftFeat_co * xcoeffs);
		totSum += err.dot(err);
		
		err = (rightFeat_co * xcoeffs - 1);
		totSum += err.dot(err);

		return totSum;
	}


	CylindricalF(cv::Mat topFeat, cv::Mat bottomFeat, cv::Mat rightFeat, cv::Mat leftFeat)
	{	
		if (bottomFeat.cols == 4 && topFeat.cols == 4 && rightFeat.cols == 4 && leftFeat.cols == 4)
		{
			bottomFeat_co = bottomFeat;
			topFeat_co = topFeat;
			rightFeat_co = rightFeat;
			leftFeat_co = leftFeat;	
		}
		else
		{
			std::cout << "Error : (line 70) Not enough cols in the feature vector " << std::endl;
		}
	}
};


//===========================================================  MAIN  ==============================================================
int main(int argc, char* argv[])
{
	if (argc < 3)
	{
		std::cout << "ERROR : program_name image_name mask_name" << std::endl;
		return -1;
	}

	cv::Mat Img_org;
	cv::Mat Img_mask;

	Img_org = cv::imread(argv[1]);
	Img_mask = cv::imread(argv[2], cv::IMREAD_GRAYSCALE);

	if (Img_mask.empty() || Img_org.empty())
	{
		std::cout << "ERROR : Images not loaded " << std::endl;
		return -2;
	}

	double img_height = Img_org.rows;
	double img_width  = Img_org.cols;
	

	//Kernels for finding the sides of the label
	cv::Mat topBorderKernel    = (cv::Mat_<double>(2, 1) << -1.0,  1.0);
	cv::Mat bottomBorderKernel = (cv::Mat_<double>(2, 1) <<  1.0, -1.0);
	cv::Mat rightBorderKernel  = (cv::Mat_<double>(1, 2) <<  1.0, -1.0);
	cv::Mat leftBorderKernel   = (cv::Mat_<double>(1, 2) << -1.0,  1.0);

	// The border labels (binary) 0 or 255
	cv::Mat topBorderLabels;
	cv::Mat bottomBorderLabels;
	cv::Mat rightBorderLabels;
	cv::Mat leftBorderLabels;


	//----------------------top--------------------------------------------
	LabelStuff::filterLargestConnectedComponent(Img_mask, topBorderKernel,topBorderLabels);

	cv::Mat topNonZeroCoords;
	cv::findNonZero(topBorderLabels, topNonZeroCoords); // NonZeroCoords.x = cols and NonZeroCoords.y = rows 

	cv::Mat topWorldCoord_x(topNonZeroCoords.total(), 1, CV_64FC1); // Along the columns
	cv::Mat topWorldCoord_y(topNonZeroCoords.total(), 1, CV_64FC1); // Along the columns

	LabelStuff::idx2zeroone(topBorderLabels, topNonZeroCoords, topWorldCoord_x, topWorldCoord_y);
	
	cv::Mat topFeatures(topNonZeroCoords.total(), 4, CV_64FC1);
	for (size_t i = 0; i < topNonZeroCoords.total(); i++)
	{
		topFeatures.at<double>(i, 0) = 1;
		topFeatures.at<double>(i, 1) = topWorldCoord_y.at<double>(i);
		topFeatures.at<double>(i, 2) = topWorldCoord_x.at<double>(i);
		topFeatures.at<double>(i, 3) = pow(topWorldCoord_x.at<double>(i),2) ;
	}
	
	
	//---------------------------------bottom------------------------------------------
	LabelStuff::filterLargestConnectedComponent(Img_mask, bottomBorderKernel, bottomBorderLabels);

	cv::Mat bottomNonZeroCoords;
	cv::findNonZero(bottomBorderLabels, bottomNonZeroCoords);

	cv::Mat bottomWorldCoord_x(bottomNonZeroCoords.total(), 1,CV_64FC1);
	cv::Mat bottomWorldCoord_y(bottomNonZeroCoords.total(), 1, CV_64FC1);

	LabelStuff::idx2zeroone(bottomBorderLabels, bottomNonZeroCoords, bottomWorldCoord_x, bottomWorldCoord_y);
	
	cv::Mat bottomFeatures(bottomNonZeroCoords.total(), 4, CV_64FC1);
	for (size_t i = 0; i < bottomNonZeroCoords.total(); i++)
	{
		bottomFeatures.at<double>(i, 0) = 1;
		bottomFeatures.at<double>(i, 1) = bottomWorldCoord_y.at<double>(i);
		bottomFeatures.at<double>(i, 2) = bottomWorldCoord_x.at<double>(i);
		bottomFeatures.at<double>(i, 3) = pow(bottomWorldCoord_x.at<double>(i),2) ;
	}


	//----------------------------------right-------------------------------------------
	LabelStuff::filterLargestConnectedComponent(Img_mask, rightBorderKernel, rightBorderLabels);
		
	cv::Mat rightNonZeroCoords;
	cv::findNonZero(rightBorderLabels, rightNonZeroCoords);

	cv::Mat rightWorldCoord_x(rightNonZeroCoords.total(), 1, CV_64FC1);
	cv::Mat rightWorldCoord_y(rightNonZeroCoords.total(), 1, CV_64FC1);

	LabelStuff::idx2zeroone(rightBorderLabels, rightNonZeroCoords, rightWorldCoord_x, rightWorldCoord_y);

	cv::Mat rightFeatures(rightNonZeroCoords.total(), 4, CV_64FC1);
	for (size_t i = 0; i < rightNonZeroCoords.total(); i++)
	{
		rightFeatures.at<double>(i, 0) = 1;
		rightFeatures.at<double>(i, 1) = rightWorldCoord_x.at<double>(i);
		rightFeatures.at<double>(i, 2) = rightWorldCoord_y.at<double>(i);
		rightFeatures.at<double>(i, 3) = rightWorldCoord_x.at<double>(i) * rightWorldCoord_y.at<double>(i);
	}


	//----------------------------------left -------------------------------------------
	LabelStuff::filterLargestConnectedComponent(Img_mask, leftBorderKernel, leftBorderLabels);
	
	cv::Mat leftNonZeroCoords;
	cv::findNonZero(leftBorderLabels, leftNonZeroCoords);

	cv::Mat leftWorldCoord_x(leftNonZeroCoords.total(), 1, CV_64FC1);
	cv::Mat leftWorldCoord_y(leftNonZeroCoords.total(), 1, CV_64FC1);

	LabelStuff::idx2zeroone(leftBorderLabels, leftNonZeroCoords, leftWorldCoord_x, leftWorldCoord_y);

	cv::Mat leftFeatures(leftNonZeroCoords.total(), 4, CV_64FC1);
	for (size_t i = 0; i < leftNonZeroCoords.total(); i++)
	{
		leftFeatures.at<double>(i, 0) = 1;
		leftFeatures.at<double>(i, 1) = leftWorldCoord_x.at<double>(i);
		leftFeatures.at<double>(i, 2) = leftWorldCoord_y.at<double>(i);
		leftFeatures.at<double>(i, 3) = leftWorldCoord_x.at<double>(i) * leftWorldCoord_y.at<double>(i);
	}
	
	
	//------------------------------------------FINDING COEFFS----------------------------------------------------
	cv::Ptr<cv::DownhillSolver> solver = cv::DownhillSolver::create(); // creating a downhill solver object				  
	cv::Ptr<cv::MinProblemSolver::Function> ptr_F = cv::makePtr<CylindricalF>(topFeatures, bottomFeatures, rightFeatures, leftFeatures); // creating a cv::Ptr to the class CylindricalF

    cv::TermCriteria termCrit; // min accuracy

	cv::Mat x = 0.0000001*(cv::Mat_<double>(1, 8) << 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0); // initializing the starting centroid of the simplex (initial point)
	cv::Mat step = 50.0*(cv::Mat_<double>(8, 1) << 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0); // the sizes of the simplex
	LabelStuff::setSolverStuff(solver, ptr_F, x, step, termCrit,1);     // Initializing the solver and running the minimization			

	// The Mathematica Coefficients
		cv::Mat txm_coeffs_out = (cv::Mat_<double>(1, 8) << 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0); // initializing the starting centroid of the simplex (initial point)
		txm_coeffs_out.at<double>(0) = -0.168207;
		txm_coeffs_out.at<double>(1) =  0.00426421;
		txm_coeffs_out.at<double>(2) =  0.000173044;
		txm_coeffs_out.at<double>(3) = -1.42875 * powf(10, -6);
		txm_coeffs_out.at<double>(4) = -0.253582;
		txm_coeffs_out.at<double>(5) =  0.00466717;
		txm_coeffs_out.at<double>(6) =  0.00175828;
		txm_coeffs_out.at<double>(7) = -6.18825 * powf(10, -6);
    
		std::cout << "\n The mathematica coeff: \n" << txm_coeffs_out.t()<< std::endl;


	if (0) // the coeffs from mathematica
	{
		x = txm_coeffs_out;
	}
	
	//--------------------------------------------APPLYING TRANSFORM ( TEST ) -----------------------------------------------------
	if (0) 
	{
		cv::Mat topTxmedCoord_x(topNonZeroCoords.total(), 1, CV_64FC1), topTxmedCoord_y(topNonZeroCoords.total(), 1, CV_64FC1);
		LabelStuff::cylTransform(topNonZeroCoords, x, topWorldCoord_x, topWorldCoord_y, topTxmedCoord_x, topTxmedCoord_y);

		cv::Mat rightTxmedCoord_x(rightNonZeroCoords.total(), 1, CV_64FC1), rightTxmedCoord_y(rightNonZeroCoords.total(), 1, CV_64FC1);
		LabelStuff::cylTransform(rightNonZeroCoords, x, rightWorldCoord_x, rightWorldCoord_y, rightTxmedCoord_x, rightTxmedCoord_y);

		cv::Mat bottomTxmedCoord_x(bottomNonZeroCoords.total(), 1, CV_64FC1), bottomTxmedCoord_y(bottomNonZeroCoords.total(), 1, CV_64FC1);
		LabelStuff::cylTransform(bottomNonZeroCoords, x, bottomWorldCoord_x, bottomWorldCoord_y, bottomTxmedCoord_x, bottomTxmedCoord_y);

		cv::Mat leftTxmedCoord_x(leftNonZeroCoords.total(), 1, CV_64FC1), leftTxmedCoord_y(leftNonZeroCoords.total(), 1, CV_64FC1);
		LabelStuff::cylTransform(leftNonZeroCoords, x, leftWorldCoord_x, leftWorldCoord_y, leftTxmedCoord_x, leftTxmedCoord_y);
	}	

	//---------------------------------------- APPLYING THE FORWARD TRANSFORMATION ----------------------------------------------
	cv::Mat Img_orig;
	Img_orig = cv::imread(argv[1], cv::IMREAD_COLOR);

	cv::Mat txmedImg;
	txmedImg.create(cv::Size(img_width, img_height), Img_orig.type());
	//txmedImg.create(cv::Size(800,800), Img_orig.type());

	cv::Mat map_x;
	map_x.create(Img_mask.size(), CV_32FC1);

	cv::Mat map_y;
	map_y.create(Img_mask.size(), CV_32FC1);

	cv::Mat x_prime(Img_orig.rows, Img_orig.cols, CV_32FC1);
	cv::Mat y_prime(Img_orig.rows, Img_orig.cols, CV_32FC1);

	cv::Mat txm_coeffs;
	txm_coeffs = x;
	txm_coeffs.convertTo(txm_coeffs,CV_32FC1);

	std::cout << "\n Coeffs in float (from double)" << txm_coeffs.t() << std::endl;
	

	for (int jj = 0; jj < Img_orig.rows; jj++)
	{
		for (int ii = 0; ii < Img_orig.cols; ii++)
		{
			auto map_x = [txm_coeffs](float x, float y) {
				return txm_coeffs.at<float>(0) + txm_coeffs.at<float>(1)*x + txm_coeffs.at<float>(2)*y + txm_coeffs.at<float>(3)*x*y;
			};

			auto map_y = [txm_coeffs](float x, float y) {
				return txm_coeffs.at<float>(4) + txm_coeffs.at<float>(5)*y + txm_coeffs.at<float>(6)*x + txm_coeffs.at<float>(7)*x*x;
			};

			x_prime.at<float>(jj, ii) = map_x(ii, jj);
			y_prime.at<float>(jj, ii) = map_y(ii, jj);			
		}
	}
	
	double min_x;
	double max_x;
	cv::minMaxIdx(x_prime, &min_x, &max_x);

	double min_y;
	double max_y;
	cv::minMaxIdx(y_prime, &min_y, &max_y);

	x_prime = img_width * (x_prime - min_x) / (max_x - min_x) ;
	y_prime = (img_height * (y_prime - min_y) / (max_y - min_y));
	
	cv::remap(Img_org, txmedImg, x_prime, y_prime, cv::INTER_LINEAR, cv::BORDER_CONSTANT, cv::Scalar(0, 0, 0));
	cv::imshow("final", txmedImg);
	cv::imshow("orig", Img_org);


	cv::waitKey();
	short temp;
	std::cin >> temp;
	return 0;
}