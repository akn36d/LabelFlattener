// part1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <opencv2\core\optim.hpp>
#include "opencv2\opencv.hpp"
#include "opencv2\imgcodecs.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"

#include <fstream>
#include <string>
#include <sstream>
#include <array>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <algorithm>

// the number of coefficients
const int num_feats_x = 4;
const int num_feats_y = 3;
const int num_coeffs_com = 10;

// taylor series expansion of asine upto 10 terms
double arcSine(double x) {
	double temp = (x
		+ (1.0 / 6.0) * pow(x, 3)
		+ (3.0 / 40.0) * pow(x, 5)
		+ (5.0 / 112.0) * pow(x, 7)
		+ (35.0 / 1152.0) * pow(x, 9)
		+ (63.0 / 2816.0) * pow(x, 11)
		+ (231.0 / 13312.0) * pow(x, 13)
		+ (143.0 / 10240.0) * pow(x, 15)
		+ (6435.0 / 557056.0) * pow(x, 17)
		+ (12155.0 / 1245184.0) * pow(x, 19));
	return temp;
}

// Defining a CylindricalF class which has the function we want to minimize---------------------------------------------------------------------------------------------------------------------------
class CylindricalF : public cv::MinProblemSolver::Function
{
public:
	cv::Mat topFeat_co; // creating a vector to hold all the coeffs since we cant just have 11 separate vars to hold the coeffs limit is 10
	cv::Mat bottomFeat_co;
	cv::Mat rightFeat_co;
	cv::Mat leftFeat_co;

	int getDims() const { return num_coeffs_com; };

	double calc(const double* coeffs) const
	{
		double totSum(0);

		cv::Mat c(std::vector<double>(coeffs, coeffs + num_coeffs_com), true);
		double topTotalCostFunc(0), bottomTotalCostFunc(0), rightTotalCostFunc(0), leftTotalCostFunc(0);
		double temp(0);

		//---------------------------Right and Left (x map)---------------------------------
		for (unsigned int i = 0; i < rightFeat_co.rows; i++)
		{
			double temp =
				(rightFeat_co.at<double>(i, 0) * c.at<double>(0))
				+ ((arcSine(rightFeat_co.at<double>(i, 1) - c.at<double>(9)) / c.at<double>(8)) * c.at<double>(1))
				+ (rightFeat_co.at<double>(i, 2) * c.at<double>(2))
				+ (rightFeat_co.at<double>(i, 3) * c.at<double>(3)) - 1;
			rightTotalCostFunc += temp*temp;
		}

		for (unsigned int i = 0; i < leftFeat_co.rows; i++)
		{
			double temp = (leftFeat_co.at<double>(i, 0) * c.at<double>(0))
				+ ((arcSine(leftFeat_co.at<double>(i, 1) - c.at<double>(9)) / c.at<double>(8)) * c.at<double>(1))
				+ (leftFeat_co.at<double>(i, 2) * c.at<double>(2))
				+ (leftFeat_co.at<double>(i, 3) * c.at<double>(3));
			leftTotalCostFunc += temp*temp;
		}

		//-----------------------------top and bottom----------------------------------
		// val - 1, y, x
		// loc - 0, 1, 2
		for (unsigned int i = 0; i < topFeat_co.rows; i++)
		{
			double clipT = pow(c.at<double>(8), 2) - pow(topFeat_co.at<double>(i, 2) - c.at<double>(9), 2);
			double clip = sqrt((clipT > 0) ? clipT : 0.01); // the ternary operator ?: removes the possibility of getting an imaginary number

			double temp = (topFeat_co.at<double>(i, 0) * c.at<double>(4))
				+ (topFeat_co.at<double>(i, 1) * c.at<double>(5))
				+ (clip * c.at<double>(6))
				+ (topFeat_co.at<double>(i, 1) * clip * c.at<double>(7)) - 1;
			topTotalCostFunc += temp*temp;
		}

		for (unsigned int i = 0; i < bottomFeat_co.rows; i++)
		{
			double clipT = pow(c.at<double>(8), 2) - pow(bottomFeat_co.at<double>(i, 2) - c.at<double>(9), 2);
			double clip = sqrt((clipT > 0) ? clipT : 0.01);

			double temp = (bottomFeat_co.at<double>(i, 0) * c.at<double>(4))
				+ (bottomFeat_co.at<double>(i, 1) * c.at<double>(5))
				+ (clip * c.at<double>(6))
				+ (bottomFeat_co.at<double>(i, 1) * clip * c.at<double>(7));
			bottomTotalCostFunc += temp*temp;

		}

		totSum = topTotalCostFunc + bottomTotalCostFunc + rightTotalCostFunc + leftTotalCostFunc;
		return totSum;
	}


	CylindricalF(cv::Mat topFeat, cv::Mat bottomFeat, cv::Mat rightFeat, cv::Mat leftFeat)
	{
		if (bottomFeat.cols == num_feats_y && topFeat.cols == num_feats_y  && rightFeat.cols == num_feats_x  && leftFeat.cols == num_feats_x)
		{
			bottomFeat.convertTo(bottomFeat_co, CV_64F);
			topFeat.convertTo(topFeat_co, CV_64F);
			rightFeat.convertTo(rightFeat_co, CV_64F);
			leftFeat.convertTo(leftFeat_co, CV_64F);
		}
		else
		{
			std::cout << "Error : (line 70) Not enough cols in the feature vector " << std::endl;
		}
	}
};

// Function to find the largest connected component in a given image
cv::Mat findLargestCC(const cv::Mat& src)
{
	cv::Mat labels;
	cv::Mat stats;
	cv::Mat centroids;
	int nLabels = cv::connectedComponentsWithStats(src, labels, stats, centroids, 8, CV_32S);

	// stats - will be a (5 x nLabels) matrix with  each column as the following properties (left,top, widhth,height, area )
	cv::Mat stat_areas = stats.col(cv::CC_STAT_AREA).rowRange(1, nLabels); // obtaining the area stats and ignoring the background area value

	cv::Point maxAreaLoc;

	cv::minMaxLoc(stat_areas, nullptr, nullptr, nullptr, &maxAreaLoc);

	cv::Mat largestCC = (labels == maxAreaLoc.y + 1);

	return largestCC;
}

// Function to apply filtering operation and then finding the largest connected component
cv::Mat largestCCWithKernel(const cv::Mat& src, const cv::Mat& BorderKernel) {
	cv::Mat border;
	cv::filter2D(src, border, -1, BorderKernel, cv::Point(-1, -1), 0, cv::BORDER_DEFAULT);

	return findLargestCC(border);
}

// Function to calculate the feature values
cv::Mat y_features(int y, int x)
{
	cv::Mat yFeats = (cv::Mat_<float>(1, num_feats_y) << 1, y, x);
	return yFeats;
}

cv::Mat x_features(int y, int x)
{
	cv::Mat xFeats = (cv::Mat_<float>(1, num_feats_x) << 1, x, y, x*y);
	return xFeats;
}



void setSolverStuff(cv::Ptr<cv::DownhillSolver> solver, cv::Ptr<cv::MinProblemSolver::Function> ptr_F, cv::Mat& x_initCentroid, cv::Mat& step, int useTermCrit)
{
	solver->setFunction(ptr_F); // solver is a DownhillSolver object pointer used to set and use all the basic DownhillSolver public member vars and member funcs  
	solver->setInitStep(step); // Sets the initial step that will be used in downhill simplex algorithm. 
	cv::TermCriteria termCrit;

	if (useTermCrit == 1) {
		termCrit = solver->getTermCriteria();
		termCrit.type = cv::TermCriteria::COUNT + cv::TermCriteria::EPS;
		termCrit.maxCount = INT_MAX;
		termCrit.epsilon = FLT_EPSILON;

		solver->setTermCriteria(termCrit);

	}

	double result = solver->minimize(x_initCentroid);  // actually runs the algorithm and performs the minimization.
													   // The sole input parameter ' x_init_centroid'  determines the centroid of the starting simplex(roughly, it tells where to start)
													   // result - holds the value of the function to be minimized after the minimization 
}

//===========================================================  MAIN  ==============================================================
int main(int argc, char* argv[])
{
	if (argc < 3)
	{
		std::cout << "ERROR : program_name image_name mask_name" << std::endl;
		return -1;
	}

	cv::Mat Img_org;
	cv::Mat Img_mask;

	Img_org = cv::imread(argv[1]);
	Img_mask = 255 * (cv::imread(argv[2], cv::IMREAD_GRAYSCALE) >  100);

	if (Img_mask.empty() || Img_org.empty())
	{
		std::cout << "ERROR : Images not loaded " << std::endl;
		return -2;
	}

	double img_height = Img_org.rows;
	double img_width = Img_org.cols;

	//Kernels for finding the sides of the label
	cv::Mat topBorderKernel = (cv::Mat_<float>(2, 1) << -1.0, 1.0);
	cv::Mat bottomBorderKernel = (cv::Mat_<float>(2, 1) << 1.0, -1.0);
	cv::Mat rightBorderKernel = (cv::Mat_<float>(1, 2) << 1.0, -1.0);
	cv::Mat leftBorderKernel = (cv::Mat_<float>(1, 2) << -1.0, 1.0);

	// The border labels (binary) 0 or 255
	cv::Mat topBorderLabels = largestCCWithKernel(Img_mask, topBorderKernel);
	cv::Mat bottomBorderLabels = largestCCWithKernel(Img_mask, bottomBorderKernel);
	cv::Mat rightBorderLabels = largestCCWithKernel(Img_mask, rightBorderKernel);
	cv::Mat leftBorderLabels = largestCCWithKernel(Img_mask, leftBorderKernel);

	// Creating the feature matrix
	cv::Mat topFeatures(cv::countNonZero(topBorderLabels), num_feats_y, CV_32F, cv::Scalar::all(0));
	cv::Mat bottomFeatures(cv::countNonZero(bottomBorderLabels), num_feats_y, CV_32F, cv::Scalar::all(0));
	cv::Mat rightFeatures(cv::countNonZero(rightBorderLabels), num_feats_x, CV_32F, cv::Scalar::all(0));
	cv::Mat leftFeatures(cv::countNonZero(leftBorderLabels), num_feats_x, CV_32F, cv::Scalar::all(0));

	int topBorderLabels_idx = 0;
	int bottomBorderLabels_idx = 0;
	int rightBorderLabels_idx = 0;
	int leftBorderLabels_idx = 0;

	// vars to hold means
	double topMean(0);
	double bottomMean(0);
	double rightMean(0);
	double leftMean(0);

	// Var to count number of non-zero elements in the borders
	int topCnt(0);
	int bottomCnt(0);
	int rightCnt(0);
	int leftCnt(0);

	// Finding all the features 
	for (int y = 0; y<Img_mask.rows; ++y)
	{
		for (int x = 0; x<Img_mask.cols; ++x)
		{
			if (topBorderLabels.at<unsigned char>(y, x) != 0)
			{
				topFeatures.row(topBorderLabels_idx++) = y_features(y, x) + 0;
				topMean += y;
				topCnt++;
			}

			if (bottomBorderLabels.at<unsigned char>(y, x) != 0)
			{
				bottomFeatures.row(bottomBorderLabels_idx++) = y_features(y, x) + 0;
				bottomMean += y;
				bottomCnt++;
			}

			if (rightBorderLabels.at<unsigned char>(y, x) != 0)
			{
				rightFeatures.row(rightBorderLabels_idx++) = x_features(y, x) + 0;
				rightMean += x;
				rightCnt++;
			}

			if (leftBorderLabels.at<unsigned char>(y, x) != 0)
			{
				leftFeatures.row(leftBorderLabels_idx++) = x_features(y, x) + 0;
				leftMean += x;
				leftCnt++;
			}
		}
	}

	topMean /= topCnt;
	bottomMean /= bottomCnt;
	rightMean /= rightCnt;
	leftMean /= leftCnt;


	//-----------------------------------Applying the Downhill Solver to find the Solution------------------------------
	cv::Ptr<cv::DownhillSolver> solver = cv::DownhillSolver::create();
	cv::Ptr<cv::MinProblemSolver::Function> ptr_F = cv::makePtr<CylindricalF>(topFeatures, bottomFeatures, rightFeatures, leftFeatures);


	// Creating a for loop to iterate over different sets of x(initial centroids) and step 
	double x_delta = 0.1; // steo size for the initial centroid iteration
	double step_delta = 5; // step size for the step iteration

						   // Multiplying factors that we increment using the above steps to get different starting centroids and steps
	double x_mul = 0.01;
	double step_mul = 5.0;

	//cv::Mat x = 0.01 * (cv::Mat_<double>(1, num_coeffs_com) << 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0); // initial point 


	// scaling factors for the sizes of the final txmed images
	float x_scaleFactor = 1;
	float y_scaleFactor = 1;

	// init the images for the txmed image and txmed mask
	cv::Mat  txmedImg(cv::Size(x_scaleFactor * img_width, y_scaleFactor * img_height), Img_org.type());
	cv::Mat txmedMask(cv::Size(x_scaleFactor * img_width, y_scaleFactor * img_height), Img_mask.type());

	//init the mapping matries in x and y for the remap() 
	cv::Mat x_map(cv::Size(x_scaleFactor * img_width, y_scaleFactor * img_height), CV_32FC1);
	cv::Mat y_map(cv::Size(x_scaleFactor * img_width, y_scaleFactor * img_height), CV_32FC1);

	// using a file to save the values for x to use later to perform an average of the best solutions of x	
	std::ofstream  myfile;
	myfile.open("coeffVals.txt");

	for (double x_mul_delta = (x_mul + x_delta);
		x_mul_delta < 1.0;
		x_mul_delta += x_delta)
	{
		for (double step_mul_delta = (step_mul + step_delta);
			step_mul_delta < 100;
			step_mul_delta += step_delta)
		{
			cv::Mat step = step_mul_delta * (cv::Mat_<double>(num_coeffs_com, 1) << 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0); // the sizes of the simplex
			cv::Mat    x = x_mul_delta * (cv::Mat_<double>(1, num_coeffs_com) << 0.0,
				(rightMean - leftMean),
				0.0,
				0.0,
				topMean,
				(bottomMean - topMean),
				0.0,
				0.0,
				(rightMean - leftMean),
				(leftMean + rightMean) / 2); // initializing the starting centroid of the simplex (initial point)

			std::cout << "\n x = " << (x_mul_delta) << "       " << "step = " << (step_mul_delta);

			try {
				setSolverStuff(solver, ptr_F, x, step, 1);     // Initializing the solver and running the minimization			
			}
			catch (cv::Exception msg)
			{
				std::cout << "\n" << msg.what() << std::endl;
			}

			//---------------------------------------Applying the transform the image-----------------------------------
			x.convertTo(x, CV_32FC1); // conversion necessary to apply the remap()

			cv::Mat coeffs = x;
			// Creating the map matrices to use for the transformation in remap()
			for (int x_idx = 0; x_idx < txmedImg.cols; x_idx++)
			{
				for (int y_idx = 0; y_idx < txmedImg.rows; y_idx++)
				{
					x_map.at<float>(y_idx, x_idx) =
						(1 * coeffs.at<float>(0))
						+ ((arcSine(x_idx - coeffs.at<float>(9)) / coeffs.at<float>(8)) * coeffs.at<float>(1))
						+ ((y_idx)* coeffs.at<float>(2))
						+ ((y_idx)* x_idx * coeffs.at<float>(3));

					float clipT = pow(coeffs.at<float>(8), 2) - pow(x_idx - coeffs.at<float>(9), 2);
					float clip = sqrtf((clipT > 0) ? clipT : 0.01);
					y_map.at<float>(y_idx, x_idx) =
						(1 * coeffs.at<float>(4))
						+ ((y_idx)* coeffs.at<float>(5))
						+ (clip * coeffs.at<float>(6))
						+ ((y_idx)* clip * coeffs.at<float>(7));
				}
			}

			cv::normalize(x_map, x_map, 0, txmedImg.cols, cv::NORM_MINMAX, CV_32FC1);
			cv::normalize(y_map, y_map, 0, txmedImg.rows, cv::NORM_MINMAX, CV_32FC1);

			cv::remap(Img_org, txmedImg, x_map, y_map, cv::INTER_LINEAR, cv::BORDER_REPLICATE, cv::Scalar(0, 0, 0)); // flattening image
																													 //cv::imshow("corrected Image", txmedImg);

			myfile << "coeffs  = " << x << "\n";

			std::string imgName = "";
			std::stringstream info_vals;

			info_vals << "img_" << (x_mul_delta) << "_" << step_mul_delta << ".jpg";
			imgName = info_vals.str();

			cv::imwrite(imgName, txmedImg);
		}
	}

	cv::waitKey();
	std::cout << "\n********************************Excecution over********************************\n";
	short temp;
	std::cin >> temp;
	return 0;
}