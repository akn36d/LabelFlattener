#include "stdafx.h"
#include "LabelStuff.h"
#include "opencv2\opencv.hpp"

namespace LabelStuff
{
	// Function to find the Largest connectd component in a given binary image, the result is returned to another image
	void filterLargestConnectedComponent(cv::Mat& Img_mask, cv::Mat BorderKernel , cv::Mat& BorderLabels)
	{	
		cv::Mat border;
		cv::filter2D(Img_mask, border, -1, BorderKernel, cv::Point(-1, -1), 0, cv::BORDER_DEFAULT);
		cv::connectedComponents(border, BorderLabels);
		double MaxSumLabel(0);

		double LabelMax;
		cv::minMaxLoc(BorderLabels, nullptr, &LabelMax);
		//std::cout << "The value of the max value (highest label) in the labelled image : " << LabelMax;

		double  TempMaxSum(0), TempSum(0);          
		MaxSumLabel = 0;
		cv::Mat TempLabel; 

		for (int iter = 1; iter <= LabelMax; iter++)
		{
			TempLabel = (BorderLabels == iter);     
			TempSum = cv::sum(TempLabel)[0];        // sum of all pixel values of the iter'th labelled object
			if (TempSum > TempMaxSum)               // current label sum > past max sum
			{
				TempMaxSum = TempSum;
				MaxSumLabel = iter;
			}
		}
		BorderLabels = (BorderLabels == MaxSumLabel);
	}	
		


	// Function to find the nonzero coordinates in the given border/image labels and scaling them to the range [0,1]-------------------------------------
	void idx2zeroone(cv::Mat& BorderLabels, cv::Mat& NonZeroCoords, cv::Mat& WorldCoord_x,cv::Mat WorldCoord_y)
	{
		double img_height, img_width;
		img_height = static_cast<double> (BorderLabels.rows);  // along y-axis
		img_width  = static_cast<double> (BorderLabels.cols);  // along x-axis

		for (size_t i = 0; i < NonZeroCoords.total(); i++)
		{
			WorldCoord_x.at<double>(i) = (NonZeroCoords.at<cv::Point>(i).x) ;
			WorldCoord_y.at<double>(i) = (NonZeroCoords.at<cv::Point>(i).y) ;
			//WorldCoord_y.at<double>(i) = (NonZeroCoords.at<cv::Point>(i).y) ;
		}
	}
	

	// Function to find the top, bottom, left and right borders of a pillbottle label
	void labelExtractor(cv::Mat Img_mask, cv::Mat BorderKernel,cv::Mat& BorderLabels,cv::Mat & NonZeroCoords) {
		LabelStuff::filterLargestConnectedComponent(Img_mask, BorderKernel, BorderLabels);

		cv::Mat topNonZeroCoords;
		cv::findNonZero(BorderLabels, NonZeroCoords); // NonZeroCoords.x = cols and NonZeroCoords.y = rows 
	}
	
	// Function to set the attributes of the solver class ----------------------------------------------------------------------------------------------------------------------------
	void setSolverStuff(cv::Ptr<cv::DownhillSolver> solver, cv::Ptr<cv::MinProblemSolver::Function> ptr_F, cv::Mat& x_initCentroid, cv::Mat& step, cv::TermCriteria termCrit, int useTermCrit)
	{
		solver->setFunction(ptr_F); // solver is a DownhillSolver object pointer used to set and use all the basic DownhillSolver public member vars and member funcs  
		int ndim = MAX(step.cols, step.rows); // This will give the number of vars in the coefficient vector(The one we need to find) i.e. the dimension of the vector 
											  // it will be n = 8 dimensional in our case as we have c1,c2,c3,...,c8 in total 8 coeffs
		solver->setInitStep(step); // Sets the initial step that will be used in downhill simplex algorithm. 
		cv::Mat settedStep;
		solver->getInitStep(settedStep); //returns the initialstep as a n-dim row vector
										 //std::cout << "The step setted : \n\t" << step << std::endl;
		if (useTermCrit == 1) {
		
			termCrit = solver->getTermCriteria();
			termCrit.type = cv::TermCriteria::COUNT + cv::TermCriteria::EPS;
			termCrit.maxCount = INT_MAX;
			termCrit.epsilon = FLT_EPSILON;

			solver->setTermCriteria(termCrit);
		
		}

		double result = solver->minimize(x_initCentroid);  // actually runs the algorithm and performs the minimization.
														   // The sole input parameter ' x_init_centroid'  determines the centroid of the starting simplex(roughly, it tells where to start)
														   // result - holds the value of the function to be minimized after the minimization 
		
		
		std::cout << "\n\nresult:\n";
		std::cout << "--------\n" << result << std::endl;
		std::cout << "\nthe optimal coeffs:\n";
		std::cout << "--------------------\n" << x_initCentroid.t() << std::endl;
	}

	

	// Defining the function to implement the transform-----------------------------------------------------------------------------------------------------------------------------------
	void cylTransform(cv::Mat& NonZeroCoords, cv::Mat& coeff, cv::Mat& WorldCoord_x, cv::Mat& WorldCoord_y, cv::Mat& TxmedCoords_x, cv::Mat& TxmedCoords_y)
	{
		for (size_t i = 0; i < NonZeroCoords.total(); i++)
		{
			TxmedCoords_x.at<double>(i) = (coeff.at<double>(0)) + (coeff.at<double>(1) * WorldCoord_x.at<double>(i)) + (coeff.at<double>(2) * WorldCoord_y.at<double>(i)) + (coeff.at<double>(3) * WorldCoord_x.at<double>(i) * WorldCoord_y.at<double>(i));
			TxmedCoords_y.at<double>(i) = (coeff.at<double>(4)) + (coeff.at<double>(5) * WorldCoord_y.at<double>(i)) + (coeff.at<double>(6) * WorldCoord_x.at<double>(i)) + (coeff.at<double>(7) * WorldCoord_x.at<double>(i) * WorldCoord_x.at<double>(i));

		}
	}



	// Function to create the mapping matrices for the function cv::remap------------------------------------------------------------------------------------------------------------------
	void mapper(cv::Mat& map_x, cv::Mat& map_y, cv::Mat coeff, int img_rows, int img_cols)
	{
		for (int y_r = 0; y_r < img_rows; y_r++)
		{
			for (int x_c = 0; x_c < img_cols; x_c++)
			{
				float y_r_normed = static_cast<float>(y_r); //img_rows;
				float x_c_normed = static_cast<float>(x_c); //img_cols;

				map_y.at<float>(y_r, x_c) = (coeff.at<float>(0) + (coeff.at<float>(1) * y_r_normed) + (coeff.at<float>(2) * x_c_normed) + (coeff.at<float>(3) * x_c_normed * x_c_normed));
				map_x.at<float>(y_r, x_c) = (coeff.at<float>(4) + (coeff.at<float>(5) * x_c_normed) + (coeff.at<float>(6) * y_r_normed) + (coeff.at<float>(7) * y_r_normed * x_c_normed));
			}
		}
	}



	// Scaling the image values to the range [0,255]----------------------------------------------------------------------------------------------------------------------------------------
	void imagesc2CV_8U(cv::Mat& map, int img_rows, int img_cols, float scale)
	{	
		double minMap, maxMap;
		cv::minMaxLoc(map, &minMap, &maxMap);

		for (int j = 0; j < img_rows; j++)
		{
			for (int i = 0; i < img_cols; i++)
			{
				map.at<float>(j, i) = map.at<float>(j, i) - static_cast<float>(minMap);
			}
		}

		cv::minMaxLoc(map, &minMap, &maxMap);

		for (int j = 0; j < img_rows; j++)
		{
			for (int i = 0; i < img_cols; i++)
			{
				map.at<float>(j, i) = (map.at<float>(j, i) / static_cast<float>(maxMap)) * scale;
			}
		}
	}

	
}


	






